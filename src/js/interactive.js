
module.exports = () => {
    let objQuestion = [];
    let prev = ['one'];
    let JSinteractive = '';
    let blockTitle = '';
    let interctiveBlock = '';

    if (document.querySelectorAll('.JSinteractive') != undefined) {
        JSinteractive = document.querySelectorAll('.JSinteractive');
    }

    function hasClass( target, className ) {
        return new RegExp('(\\s|^)' + className + '(\\s|$)').test(target.className);
    }

    let isCurrentState = (el) => {
        let current = el.querySelector('.JSinteractive__current');
        let blockTitle = el.querySelector('.JSinteractive__title');
        let interctiveBlock = el.querySelector('.interctive_block');

        if (!hasClass(current, 'JSinteractive__sendForm')) {
            let JSsteps = current.querySelector('.JSinteractive__step');
            let JSstepsAttr = JSON.parse(JSsteps.getAttribute('data-step'));

            if (!objQuestion[JSstepsAttr.step - 1]) {
                JSsteps.setAttribute("disabled", "disabled");
            } else {
                JSsteps.removeAttribute("disabled");
            }

            blockTitle.style.display = 'block';
        } else {
            blockTitle.style.display = 'none';
            
            if (window.interactive[1].value == 'Строительство') {
                interctiveBlock.style.display = 'block';
                interctiveBlock.innerHTML = '<input class="form__input cityClient" type="text" name="City" placeholder="Ваш город" required>';
            }
        }
    };

    let interactiveEvent = (event, el) => {
        let question = JSON.parse(event.target.getAttribute('data-question'));
        let button = JSON.parse(event.target.getAttribute('data-step'));

        let inputs = [];
        let inputsOrder;

        if (question) {
            if (question.type === 'radio') {
                objQuestion[question.order] = question;
                window.interactive = objQuestion;
                isCurrentState(el);
            } else if (question.type === 'text') {
                inputs = [];
                let list = el.querySelectorAll('.JSinteractive__current .JSinteractive__list input[type="text"]');
            
                list.forEach(function(item, i) {
                    item.addEventListener("keyup", function() {
                        inputs[i] = item.value;
                    }, true);
                });
                    inputsOrder = question.order
                    question.value = inputs;
                    objQuestion[question.order] = question;
                    window.interactive = objQuestion;
                    isCurrentState(el);
            } else {
                let checked = [];
                let list = el.querySelectorAll('.JSinteractive__current .JSinteractive__list input[type="checkbox"]');

                list.forEach(function(item, i) {
                    if (item.checked) {
                        checked[i] = item.value;
                    }
                });

                if (checked.length) {
                    question.value = checked;
                    objQuestion[question.order] = question;
                    window.interactive = objQuestion;
                    isCurrentState(el);
                }
            } 
        } else if (button) {
            let thisBlock = el.querySelector('.JSinteractive__current');
            let nextBlock;
            if (button.type === 'next') {
                let target;

                if (button.stateType === "radio") {
                    target = objQuestion[button.step - 1].target;
                } else {
                    target = button.target;
                }

                nextBlock = el.querySelector('.JSinteractive__' + target);
                prev[button.step - 1] = button.state;

                
            } else {
                nextBlock = el.querySelector('.JSinteractive__' + prev[button.step] + '.JSinteractive__' + button.step);
                prev.slice(0, -1);
            }
            thisBlock.style.display = 'none';
            nextBlock.style.display = 'flex';
            thisBlock.classList.remove('JSinteractive__current');
            nextBlock.classList.add('JSinteractive__current');
            isCurrentState(el);
        }
    };

    if (document.querySelectorAll('.JSinteractive') != undefined) {
        for(let i = 0; i < JSinteractive.length; i++) {
            isCurrentState(JSinteractive[i]);
            JSinteractive[i].addEventListener("click", function() {interactiveEvent(event, JSinteractive[i]) }, false);
        }        
    }
};
