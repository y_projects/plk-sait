<?php
/*
 * @link      https://github.com/bit55/litero
 * @copyright Copyright (c) 2017 Eugene Dementyev.
 * @license   https://opensource.org/licenses/BSD-3-Clause
 */
namespace Bit55\Litero;

class MainController {

    public static function base() {
        $page = 'base';
        require_once './baseTemplate.php';
    }

    public static function sro() {
        $jsonData = 'pages/sro/sro';
        $uri = '/sro';
        require_once './baseTemplate.php';
    }

    public static function tenderSupport($any) {
        $arr = explode('/', $any);
        $uri = '/tendersupport/' . $any;

        if (isset($arr[1])) {
            $jsonData = 'pages/tenderSupport/' . $any;
            require_once './baseTemplate.php';
        } else {
            $jsonData = 'pages/tenderSupport/' . $any. '/' . $any;
            require_once './baseTemplate.php';
        }
    }

    public static function licenses() {
        $jsonData = 'pages/licenses/licenses';
        $uri = '/licenses';
        require_once './baseTemplate.php';
    }


    public static function licensesPages($any) {
        $arr = explode('/', $any);
        $uri = '/licenses/' . $any;

        if (isset($arr[1])) {
            $jsonData = 'pages/licenses/' . $any;
            require_once './baseTemplate.php';
        } else {
            $jsonData = 'pages/licenses/' . $any. '/' . $any;
            require_once './baseTemplate.php';
        }
    }

    public static function attestationPages($any) {
        $arr = explode('/', $any);
        $uri = '/attestation/' . $any;

        if (isset($arr[1])) {
            $jsonData = 'pages/attestation/' . $any;
            require_once './baseTemplate.php';
        } else {
            $jsonData = 'pages/attestation/' . $any. '/' . $any;
            require_once './baseTemplate.php';
        }
    }

    public static function personalPages($any) {
        $arr = explode('/', $any);
        $uri = '/personal/' . $any;

        if (isset($arr[1])) {
            $jsonData = 'pages/personal/' . $any;
            require_once './baseTemplate.php';
        } else {
            $jsonData = 'pages/personal/' . $any. '/' . $any;
            require_once './baseTemplate.php';
        }
    }
}

?>